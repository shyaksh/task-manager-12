package ru.bokhan.tm.model;

public class Command {

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public Command(String name, String argument) {
        this.name = name;
        this.argument = argument;
    }

    public Command(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (argument != null && !argument.isEmpty()) result.append(", ").append(argument);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}

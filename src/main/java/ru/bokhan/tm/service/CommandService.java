package ru.bokhan.tm.service;

import ru.bokhan.tm.api.repository.ICommandRepository;
import ru.bokhan.tm.api.service.ICommandService;
import ru.bokhan.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArguments() {
        return commandRepository.getArguments();
    }

}
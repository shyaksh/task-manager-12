package ru.bokhan.tm.api.service;

import ru.bokhan.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
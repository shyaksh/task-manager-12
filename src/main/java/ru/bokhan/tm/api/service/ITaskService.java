package ru.bokhan.tm.api.service;

import ru.bokhan.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
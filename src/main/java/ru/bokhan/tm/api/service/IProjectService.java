package ru.bokhan.tm.api.service;

import ru.bokhan.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
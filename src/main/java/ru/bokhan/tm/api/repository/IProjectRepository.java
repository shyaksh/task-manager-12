package ru.bokhan.tm.api.repository;

import ru.bokhan.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

}
package ru.bokhan.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

}